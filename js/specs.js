


/*
 * DIRECTIVAS
 */
describe('limitDigits', function(){
  function read (val) {
    if (!isNaN(val)) {
      return val.toFixed(2);
    } else {
      return val;
    }
  }

  it('every decimal number should be reduced to only two decimals (string)', function(){
    expect(expect(read(2.12345)).toEqual('2.12'));
  });  

  it('if the value is a string should return the same value', function(){
    expect(expect(read('word')).toEqual('word'));
  });  
});

describe('noDigits', function(){
  function read (val) {
    if (!isNaN(val)) {
      return parseInt(val);
    } else {
      return val;
    }
  }

  it('every decimal number should return a int', function(){
    expect(expect(read(2.55)).toEqual(2));
  });  

  it('if the value is a string should return the same value', function(){
    expect(expect(read('word')).toEqual('word'));
  });  
});

describe('offer-terms-summer', function(){
  var x = 2;
  var newValue;
  function update (value) {
    if (!isNaN(value)) {
      switch (value) {
        case 1:
          newValue = 'NO SUMMER';
          break;
        case 2:
          newValue = 'SUMMER';
          break;
        default:
          newValue = '';
          break;
      }
    } else {
      newValue = '';
    }
    return newValue;
  }

  it('every string passed should return a empty string', function(){
    expect(expect(update('s')).toEqual(''));
  });
  
  it('if value is 1 text will be no summer', function(){
    expect(update(1)).toBe('NO SUMMER');
  });
  
  it('if value is 1 text will be summer', function(){
    expect(update(2)).toBe('SUMMER');
  });
  
  it('any number but 1 or 2 is returns empty', function(){
    expect(update(3)).toBe('');
  });
});

describe('affinity-partner', function(){
  function update (value) {
    if (value === null || value === '') {
      var newValue = 'N/A';
      return newValue;
    } else {
      return value;
    }
  }

  it('if value is null then return N/A', function(){
    expect(expect(update(null)).toEqual('N/A'));
  });  

  it('if value is empty then return N/A', function(){
    expect(expect(update('')).toEqual('N/A'));
  });  

  it('if the value is not empty or null, then return the same value', function(){
    expect(expect(update('word')).toEqual('word'));
  });  
});








/*
 * GENERAL
 */
describe('offers', function(){

  it('right container should always exist', function(){
    expect(document.querySelector('.offers__right-container')).not.toEqual(undefined);
  });

  it('agent script should always exist', function(){
    expect(document.getElementById('agent-script')).not.toEqual(undefined);
  });

  it('data offers should never be undefined', function(){
    expect(data.offers).not.toEqual(undefined);
  });

  it('Vue instance can be accessed from outside', function(){
    expect(vm).toEqual(undefined);
  });
});