var Internal;

/*
 * Validation for undefined or empty data
 */
if (data !== undefined && data.length > 0) {
  var data = data[0];

  data.offers.forEach(function (element) {
    var randomN = Math.floor((Math.random() * 1000000) + new Date().getMilliseconds());
    element.offerId = randomN;
  });
} else {
  var data = [
    {
      "offers": []
    }
  ];
  var data = data[0];
}


/*
 * Formating autorenewal language text if it exist
 */
if (!error && data.autorenewalLanguage !== null && data.autorenewalLanguage !== undefined) {
  var newAutoRenewal = data.autorenewalLanguage.replace(/&lt;/gi, "<");
  newAutoRenewal = newAutoRenewal.replace(/&gt;/gi, ">");
  newAutoRenewal = newAutoRenewal.replace(/&quot;/gi, "'");
} else {
  var newAutoRenewal = null;
}


/*
 * Filters
 */
Vue.filter('limitDigitsPrice', {
  read: function (val) {
    if (!isNaN(val) && val % 1 !== 0) {
      if (val < 0.01) {
        return val.toPrecision(1);
      }
      return val.toFixed(2);
    }
    return val;
  }
});
Vue.filter('limitDigitsPercentage', {
  read: function (val) {
    if (!isNaN(val) && val % 1 !== 0 && val > 1) {
      return val.toFixed(2);
    } else if (!isNaN(val) && val % 1 === 0) {
      return parseInt(val);
    } else {
      return val;
    }
  }
});
Vue.filter('limitDigitsEtf', {
  read: function (val) {
    if (val.toLowerCase() == 'no') {
      return val;
    } else if (val % 1 !== 0) {
      return parseFloat(val).toFixed(2);
    } else if (val % 1 === 0) {
      return parseInt(val);
    } else {
      return val;
    }
  }
});

Vue.filter('formatNumber', {
  read: function(val) {
    return val === '' ? '' : Internal.formatNumber(val);
  },
  write: function(val, oldVal) {
    var num = val === '' ? '' : Internal.formatNumber(val);

    if (!isNaN(num) && num > 999.99) {
      this.priceHighNumberError = true;
      return 999.99;
    } else if (!isNaN(num)) {
      this.priceHighNumberError = false;
      return num;
    }
  }
});

/*
 * Returns text based on summer value
 */
Vue.directive('offer-terms-summer', {
  update: function(value) {
    var newValue;
    if (!isNaN(value)) {
      switch (value) {
        case 1:
          newValue = 'NO SUMMER';
          break;
        case 2:
          newValue = 'SUMMER';
          break;
        default:
          newValue = '';
          break;
      }
    } else {
      newValue = '';
    }
    this.el.innerHTML = newValue;
  }
});

/*
 * Returns N/A text when value is null or empty
 */
Vue.directive('affinity-partner', {
  update: function(value) {
    if (value === null || value === '') {
      var newValue = 'N/A';
      this.el.innerHTML = newValue;
    } else {
      this.el.innerHTML = value;
    }
  }
});


/*
 * Filter terms of the offer and sums it all when the value is in months, if not, show the text
 */
Vue.directive('offer-total-terms', {
  update: function(value) {
    function filterById(offer) {
      return offer.offerCode === value;
    }

    function filterByTime(term) {
      return term.type === 'time';
    }

    function filterByDate(term) {
      return term.type === 'date';
    }

    var filteredTerms = data.offers.filter(filterById);
    var timeTerms = filteredTerms[0].terms.filter(filterByTime);
    var dateTerms = filteredTerms[0].terms.filter(filterByDate);

    if (timeTerms.length > 0) {
      var totalTerms = 0;

      // Sums all terms months
      for (var i = 0; i < timeTerms.length; i++) {
        totalTerms += timeTerms[i].term;
      }

      $(this.el).html(totalTerms);
    } else if (dateTerms.length > 0) {
      $(this.el).html(dateTerms[0].term);
    }
  }
});



Internal = (function ($) {
  'use strict';

  var module = {};
  var programTypeFilters = [];
  var termFilters = [];

  var tagsMap = {
    AUTORENEWAL: {
      text: "Auto Renewal Offer",
      icon: "icon-renew-icon"
    },
    NOTICE: {
      text: "Renewal Offer",
      icon: "icon-renew-icon"
    },
    SPECIALOFFERS: {
      text: "Special Offer",
      icon: "icon-special-icon"
    }
  };

  var filtersMap = {
    NOTICE: {
      text: "Renewal Notice's Offers"
    },
    SPECIALOFFERS: {
      text: "Special Offers"
    }
  };

  module.setFilters = function() {
    var registeredProgramTypes = [];

    data.offers.forEach(function(element) {

      // Program Types
      if (element.tags !== undefined) {
        var programType = element.tags[0];
        var completedName = filtersMap[programType] ? filtersMap[programType].text : null;

        if (registeredProgramTypes.indexOf(programType) == -1 && completedName) {
          programTypeFilters.push({
            completedName: completedName,
            programType: programType
          });

          registeredProgramTypes.push(programType);
        }
      }

      // Terms
      var termFilter = [];
      if (termFilters.indexOf(parseInt(element.termBundle.filter)) == -1 && element.termBundle.filter != null) {
        termFilters.push(parseInt(element.termBundle.filter));
      }
    });
  };


  module.formatNumber = function (x) {
    return parseFloat(x).toFixed(2);
  };

  module.init = function () {
    module.setFilters();

    /*
     * Vue instance
     */
    var vm = new Vue({
      el: '#offers',
      data: {
        priceFilterSearch: {
          from: '',
          to: ''
        },
        programTypeFilters: programTypeFilters,
        programTypeSearch: [],
        termFilters: termFilters,
        selectedTermFilter: [],
        errors: data.errors,
        offers: data.offers,
        agentScript: agentScript,
        autorenewalLanguage: newAutoRenewal,
        error: error,
        filtersHidden: true,
        priceHighNumberError: false,
        selectedOffer: ''
      },
      ready: function() {
        document.getElementById('agent-script').src = this.agentScript;
        window.addEventListener("scroll", this.onscroll);

        $(".c-filter__price").numeric({
            allow   : '.',
            allowThouSep : false,
            maxDecimalPlaces    : '2',
            maxPreDecimalPlaces : '3'
        });


        /*
         * Show hidden page
         */
        document.querySelector('#offers').classList.add("offers--show");

        /*
         * Adding tooltips to all offers
         */
        module.addTooltips();


        /*
         * Start customized checkbox
         */
        $('.checkbox').checkbox()

        /*
         * Reinitializing tooltips when use the drag and drop
         */
        Vue.vueDragula.eventBus.$on('drop', function () {
          module.addTooltips();
          $(document).off('mousemove');
        });

        Vue.vueDragula.eventBus.$on('drag',function(el,source){        
            var h = $(window).height();                  
            $(document).on('mousemove', function(e) {     
                var mousePosition = e.pageY - $(document).scrollTop();
                var topRegion = 120;
                var bottomRegion = h - 120;
   
                if(e.which == 1 && (mousePosition < topRegion || mousePosition > bottomRegion)) {                 
                  var distance = e.clientY - h / 2;
                  distance = distance * 0.04; // <- velocity
                  $(document).scrollTop( distance + $(document).scrollTop()) ;                    
                }
            });
        });

      },
      watch: {
        'programTypeSearch': function () {
          module.addTooltips();
        },
        'selectedTermFilter': function () {
          module.addTooltips();
        },
        'priceFilterSearch': {
          handler: function(val, oldVal) {
            module.addTooltips();
          },
          deep: true
        }
      },
      computed: {
        noticeFailed: function () {
          for (var i in this.errors) {
            if (this.errors[i].code === 'ERROR_NOTICE_OFFER') {
              return true;
            }
          }
          return false;
        },
        allAppliedFilters: function () {
          return this.selectedTermFilter.concat(this.programTypeSearch);
        }
      },
      methods: {
        clearFilters: function () {
          this.programTypeSearch = [];
          this.selectedTermFilter = [];
          this.priceFilterSearch.from = '';
          this.priceFilterSearch.to = '';
        },
        removeFilter: function (filter) {
          var index = this.programTypeSearch.indexOf(filter);

          if (index > -1) {
            this.programTypeSearch.splice(index, 1);
          }
        },
        removeTermFilter: function (filter) {
          this.selectedTermFilter.splice(this.selectedTermFilter.indexOf(filter), 1);
        },
        termsFilter: function (offer) {
          return this.selectedTermFilter.length == 0 || this.selectedTermFilter.indexOf(offer.termBundle.filter) != -1 ?
              offer :
              false;
        },
        priceFilter: function(offer) {
          var price = parseFloat(module.formatNumber(offer.termBundle.terms[0].value));
          var priceFrom = this.priceFilterSearch.from === '' ?
                0 :
                parseFloat(module.formatNumber(this.priceFilterSearch.from));
          var priceTo = this.priceFilterSearch.to === '' ?
                0 :
                parseFloat(module.formatNumber(this.priceFilterSearch.to));

          if (offer.termBundle.terms[0].symbol === '%') {
            return offer;
          }

          /*
           * 0 - 0
           */
          if (priceFrom === 0 && priceTo === 0) {
            return offer;
          }

          /*
           * 0 - X
           */
          if (priceFrom === 0 && priceTo !== 0 && price >= 0 && price <= priceTo) {
            return offer;
          }

          /*
           * X - 0
           */
          if ((priceFrom !== '' && priceFrom !== 0) &&
              (priceTo === 0 || priceTo === '') &&
              price >= priceFrom) {
            return offer;
          }

          /*
           * X - X
           */
          if (price >= priceFrom && price <= priceTo) {
            return offer;
          }
        },
        programTypeFilter: function(offer) {
          if (this.programTypeSearch.length === 0) {
            return offer;
          } else if (offer.tags !== '' && offer.tags !== null && offer.tags !== undefined) {
            var returnOffer;
            this.programTypeSearch.forEach(function(element) {
              if (element.programType === offer.tags[0]) {
                returnOffer = offer;
              }
            });
            return returnOffer;
          }
        },
        onscroll: function () {
          /*
           * Sticky Agent script
           */
          var agent = document.querySelector('.offers__right-container');
          if (window.scrollY > 67 || document.documentElement.scrollTop > 67) {
            agent.classList.add('right-block--fixed');
          } else {
            agent.classList.remove('right-block--fixed');
          }
        },
        verifyTag: function (tag) {
          return !!tagsMap[tag];
        },       
        parseOfferName: function (offer) {
          if (this.verifyTag(offer[0])) {
            return offer.indexOf('AUTORENEWAL') != -1 ?
                tagsMap.AUTORENEWAL.text :
                tagsMap[offer[0]].text;
          }
        },
        parseOfferIcon: function (offer) {
          if (this.verifyTag(offer[0])) {
            return offer.indexOf('AUTORENEWAL') != -1 ?
                tagsMap.AUTORENEWAL.icon :
                tagsMap[offer[0]].icon;
          }
        },
        /*
         * Toggle btn for selected offer
         */
        selectOffer: function(offer) {
          if (this.selectedOffer === offer) {
            this.selectedOffer = '';
          } else {
            this.selectedOffer = offer;
            this.selectedOffer.offerCode = offer.offerCode
          }
        }
      }
    });
  };

  module.cleanString = function (value) {
    var str = value;
    str = str.replace(/[^A-Z0-9]/ig, "-");
    return str;
  };

  module.addTooltips = function () {
    $('.offer__info').each(function () {
      var dataId = $(this).attr('data-id');
      $('#offer__info-' + dataId).popup({
        popup: '#offer__tooltip-' + dataId
      });
    });
  };

  return module;
})($, window, document);


/*
 * Create Vue Instance
 */
Internal.init();